/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  StatusBar,
  useColorScheme,
  ToastAndroid,
  Platform,
  Alert
} from 'react-native';

import {Colors} from 'react-native/Libraries/NewAppScreen';
import {QuestionsBoard} from './components/QuestionsBoard';

const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter
  };

  const onSubmit = data => {
    if (Platform.OS === 'ios') {
      return Alert.alert(
        'QuestionsBoard Result',
        `q1: ${data[0].a}\nq2: ${data[1].a}`
      );
    } else {
      return ToastAndroid.show(
        `q1: ${data[0].a}\nq2: ${data[1].a}`,
        ToastAndroid.SHORT
      );
    }
  };

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <QuestionsBoard questions={['q1', 'q2']} onSubmit={onSubmit} />
    </SafeAreaView>
  );
};

export default App;
