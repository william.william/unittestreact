import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import renderer from 'react-test-renderer';
import {QuestionsBoard} from 'components/QuestionsBoard';

describe('QuestionsBoard', () => {
  const allQuestions = ['q1', 'q2'];
  it('should return as expected when form submits two answers', () => {
    const mockFn = jest.fn();

    const {getAllByA11yLabel, getByText} = render(
      <QuestionsBoard questions={allQuestions} onSubmit={mockFn} />
    );

    const answerInputs = getAllByA11yLabel('answer input');

    fireEvent.changeText(answerInputs[0], 'a1');
    fireEvent.changeText(answerInputs[1], 'a2');
    fireEvent.press(getByText('Submit'));

    expect(mockFn).toBeCalledWith({
      0: {q: 'q1', a: 'a1'},
      1: {q: 'q2', a: 'a2'}
    });
  });

  it('should render correctly', () => {
    const mockFn = jest.fn();
    const tree = renderer
      .create(<QuestionsBoard questions={allQuestions} onSubmit={mockFn} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
